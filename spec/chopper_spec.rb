require 'spec_helper'

describe 'Chopper' do
  let(:chopper) { Chopper.new }

  it 'chop de 3 y vacio deberia ser -1' do
    expect(chopper.chop(3, [])).to eq -1 # rubocop:disable Lint/AmbiguousOperator
  end

end
